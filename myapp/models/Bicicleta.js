let Bicicleta = function (id, color, modelo, ubicacion) {

    this.id = id;

    this.color = color;

    this.modelo = modelo;

    this.ubicacion = ubicacion;

}
/*
Bicicleta.allBicis = [];



Bicicleta.add = function (bici) {

    this.allBicis.push(bici);

}



Bicicleta.removeById = function(aBiciId) {//metodo para borar una bici

    for (let i=0; i< Bicicleta.allBicis.length; i++) {

        if (Bicicleta.allBicis[i].id == aBiciId) {

            Bicicleta.allBicis.splice(i,1);

            break;

        }

    }

}


Bicicleta.findById = function(aBiciId) {//metodo para actualizar una bici

    let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);

    if (aBici){

        return aBici;

    }else{

        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}



let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);

let b = new Bicicleta(2, "Azul", "Orbea", [28.501367, -13.853476]);

Bicicleta.add(a);
Bicicleta.add(b);

*/


//Requerimos de mongoose y creamos el objeto schema
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
    biciletaID: Number,
    color: String,
    modelo: String,
    ubicacion: {type:[Number], index: true}
});
// Fin de Schema

//Ejercicio 3 Tarea A16
bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    return this.create(aBici, cb);
};

//Ejercicio 5 Tarea A16
bicicletaSchema.statics.findById = function (aBiciId, cb){
    return this.findById(aBiciId,cb);
};

bicicletaSchema.static.removeById = function(aBiciId, cb){
    return this.removeById(aBiciId, cb);
};

//Dice que el nuevo export sustituye al anterior. Preguntar si esta bien lo hecho
//module.exports = Bicicleta;
module.exports = mongoose.model ("Bicicleta", bicicletaSchema);
