
let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function(req,res){
     res.render("bicicletas/index", {bicis: Bicicleta.allBicis});

}


exports.bicicleta_create_get = function(req,res) {

    res.render("bicicletas/create");//metodo get para mostrar el formulario

}



exports.bicicleta_create_post = function(req,res) { //metodo post para envío del formulario

    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);

    bici.ubicacion = [req.body.latitud, req.body.longitud];

    Bicicleta.add(bici);

    res.redirect("/bicicletas"); 
}



exports.bicicleta_delete_post = function(req,res) {//metodo post para borrar una bicicleta

    Bicicleta.removeById(req.body.id);
   
    res.redirect("/bicicletas");
   
}

   
exports.bicicleta_update_get = function(req,res) {//metodo get para actualizar una bicicleta

    let bici = Bicicleta.findById(req.params.id);

    res.render("bicicletas/update", {bici});
}

exports.bicicleta_update_post = function(req,res) {//metodo post para actualizar una bicicleta

    let bici = Bicicleta.findById(req.params.id);
    bici.id = [req.body.id];
    bici.color = [req.body.color];
    bici.modelo = [req.body.modelo];
    bici.ubicacion = [req.body.latitud, req.body.longitud];

    Bicicleta.findById(req.params.id);

    res.redirect("/bicicletas");


}


// Ejercicio 4 A16
exports.bicicleta_list = function (req,res){
    Bicicleta.allBicis(function (err, bicis) {
        if (err) res.status(500).send(err.message);

        res.status(200).json({
            bicicletas: bicis
        })
    })
}

exports.bicicleta_create = function (req,res) {
    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });

    Bicicleta.add(bici,function (err,newBici){
        if (err) res.status(500).send(err.mensage);
        res.status(201).send(newBici);
    });
};


// Ejercicio 5 Tarea A16
exports.bicicleta_findById = function(req,res){
    Bicicleta.findById(req.params.id, function(err,aBici){
        if (err) res.status(500).send(err.mensage);
        res.status(201).send(aBici);
    });
};

exports.biciclera_removeById = function(req,res){
    Bicicleta.removeById(req.params.id, function(err,aBici){
        if(err) res.status(500).send(err.mensage);
        res.status(201).send(aBici);
    });
};