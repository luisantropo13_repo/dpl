var calculadora = require("./calculadora.js");
var fecha = require("./fecha.js");

// Ejercicio 1
// Apartado 1
console.log("La suma de tus numeros es "+calculadora.suma(2,2));
console.log("La resta de tus numeros es "+ calculadora.resta(5,3));
console.log("La multiplicación de tus numeros es "+ calculadora.multip(3,3));
console.log("La división de tus numeros es "+ calculadora.divi(10,2));
console.log("El modulos de tus numeros es "+ calculadora.modu(20,3));

//Apartado 2
console.log("Son las "+fecha.fecha());                                

//Apartado 3
var fechaPuerto = Date();
console.log(fechaPuerto);

var puerto = 3000;
var http = require("http");

http.createServer(function(req,res) {
    res.writeHead(200,{"Content-Type": "text/html"});
    res.end(puerto);
}).listen(puerto);

console.log("El Puerto es "+puerto);
